from flask import Flask,request ,render_template
app = Flask(__name__)

# @signifies a decorator - way to wrap a function and modifying its behavior
# https://www.youtube.com/watch?v=8aTnmsDMldY (login)
""" @app.route('/')
@app.route('/<user>')
def index(user=None):
  return render_template('profile.html',user=user)
"""
@app.route('/')
def index():
   #return 'this is the homepage Method used %s' %request.method
   return render_template('index.html')
   
@app.route('/about/',methods=['GET','POST'])
def about():
   if request.method == 'POST':
      return 'yes its POST method'
   else:
      return 'yes its GTE method'

@app.route('/profile/<username>')
def profile(username):
  return render_template('profile.html',name=username)



@app.route('/post/<int:post_id>')
def post(post_id):
   return 'its about the page %s' % post_id

if __name__ == '__main__':
   app.run(debug = True)